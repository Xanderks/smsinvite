﻿namespace SmsInvite.Exceptions
{
    using System;

    /// <summary>
    /// Default exception
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ApiException: Exception
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public string Code { get; set; }
        /// <summary>
        /// Gets a message that describes the current exception.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiException"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="message">The message.</param>
        public ApiException(string code, string message)
        {
            this.Code = code;
            this.Message = message;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiException"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="message">The message.</param>
        /// <param name="module">The module.</param>
        public ApiException(string code, string message, string module)
        {
            this.Code = code;
            this.Message = $"INTERNAL {module}: {message}";
        }
    }
}
