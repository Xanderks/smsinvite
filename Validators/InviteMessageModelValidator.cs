﻿namespace SmsInvite.Validators
{
    using FluentValidation;
    using Microsoft.Extensions.Localization;
    using SmsInvite.Models;
    using SmsInvite.Utils;
    using System.Linq;
    using System.Net;

    /// <summary>
    /// InviteMessageModel validator
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{InviteMessageModel}" />
    public class InviteMessageModelValidator: AbstractValidator<InviteMessageModel>
    {
        public InviteMessageModelValidator(IStringLocalizer<Startup> localizer)
        {
            
            RuleForEach(x => x.PhoneNumbers).Matches("^7[0-9]{10}$").WithMessage(x => localizer["Error400"]).WithErrorCode(((int)HttpStatusCode.BadRequest).ToString());
            
            RuleFor(x => x.PhoneNumbers).NotNull().NotEmpty().WithMessage(x => localizer["Error401"]).WithErrorCode(((int)HttpStatusCode.Unauthorized).ToString());
            
            RuleFor(x => x.PhoneNumbers.Length).Must(x => x <= 16).WithMessage(x => localizer["Error402"]).WithErrorCode(((int)HttpStatusCode.PaymentRequired).ToString());
            
            RuleFor(x => x.PhoneNumbers.Length - x.PhoneNumbers.Distinct().Count()).Must(x => x == 0).WithMessage(x => localizer["Error404"]).WithErrorCode(((int)HttpStatusCode.NotFound).ToString());
            
            RuleFor(x => x.Message).NotEmpty().WithMessage(x => localizer["Error405"]).WithErrorCode(((int)HttpStatusCode.MethodNotAllowed).ToString());
            
            RuleFor(x => string.IsNullOrEmpty(x.Message)? true : x.IsMessageCyrillic ? x.Message.Length <= 128: x.Message.Length <= 160).Must(x => x == true).WithMessage(x => localizer["Error407"]).WithErrorCode(((int)HttpStatusCode.ProxyAuthenticationRequired).ToString());
            
            RuleFor(x => GsmEncodingChecker.CheckMessage(x.TranslatedMessage)).Must(x => x == true).WithMessage(x => localizer["Error406"]).WithErrorCode(((int)HttpStatusCode.NotAcceptable).ToString());
        }
    }
}
