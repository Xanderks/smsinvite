﻿namespace SmsInvite.Utils
{
    /// <summary>
    /// GSM encoding util
    /// </summary>
    public static class GsmEncodingChecker
    {
        private static string allowedSymbols = "@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞ\x1bÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";

        public static bool CheckMessage(string message)
        {
            if (message == null)
            {
                return true;
            }

            foreach(var let in message)
            {
                if (allowedSymbols.IndexOf(let) == -1 )
                {
                    return false;
                }
            }

            return true;

        }
    }
}
