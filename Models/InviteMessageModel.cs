﻿namespace SmsInvite.Models
{
    using SmsInvite.Utils;

    /// <summary>
    /// Invite message
    /// </summary>
    public class InviteMessageModel
    {
        private string _translatedMessage;
        private string _originalMessage;

        public bool IsMessageCyrillic => _translatedMessage != _originalMessage;

        /// <summary>
        /// Gets or sets the phones.
        /// </summary>
        /// <value>
        /// The phones.
        /// </value>
        public string[] PhoneNumbers { get; set;}

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { 
            get { return this._originalMessage; }
            set { 
                this._originalMessage = value; 
                this._translatedMessage = LatinConverter.ConvertToLatin(value); 
            } 
        }
        /// <summary>
        /// Gets the translated message.
        /// </summary>
        /// <value>
        /// The translated message.
        /// </value>
        public string TranslatedMessage => this._translatedMessage;
    }
}
