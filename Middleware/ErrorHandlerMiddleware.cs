﻿namespace SmsInvite.Middleware
{
    using Microsoft.AspNetCore.Http;
    using SmsInvite.Exceptions;
    using System;
    using System.Net;
    using System.Threading.Tasks;

    /// <summary>
    /// Global exception handler
    /// </summary>
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                var result = "";

                switch (error)
                {
                    case ApiException e:
                        response.StatusCode = int.Parse(e.Code);
                        result = e.Message;
                        break;
                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        result = $"INTERNAL SMS_SERVICE: {error.Message}";
                        break;
                }

                await response.WriteAsync(result);
            }
        }
    }
}
