﻿namespace SmsInvite.Services
{
    using FluentValidation;
    using Microsoft.Extensions.Localization;
    using Microsoft.Extensions.Logging;
    using SmsInvite.DAL;
    using SmsInvite.Exceptions;
    using SmsInvite.Models;
    using SmsInvite.Validators;
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    /// <summary>
    /// Auth service implementation
    /// </summary>
    /// <seealso cref="SmsInvite.Services.IAuthService" />
    public class AuthService : IAuthService
    {
        private const string moduleName = "AuthService";
        private const int maxInvitations = 128;

        private readonly ILogger<AuthService> logger;
        private readonly IValidator<InviteMessageModel> validator;
        private readonly ISmsRepository smsRepository;
        private readonly IStringLocalizer<Startup> localizer;

        public AuthService(ILogger<AuthService> logger, IValidator<InviteMessageModel> validator, 
                           ISmsRepository smsRepository, IStringLocalizer<Startup> localizer)
        {
            this.logger = logger;
            this.validator = validator;
            this.smsRepository = smsRepository;
            this.localizer = localizer;
        }

        public async Task<int> SendInvitesAsync(InviteMessageModel inviteMessage, int userId)
        {
            try
            {
                var res = validator.Validate(inviteMessage);

                if (!res.IsValid)
                {
                    var error = res.Errors.FirstOrDefault();
                    throw new ApiException(error.ErrorCode, error.ErrorMessage);
                }

                int invCount = await this.smsRepository.GetCountInvitationsAsync();
                if (invCount > maxInvitations)
                {
                    throw new ApiException(((int)HttpStatusCode.Forbidden).ToString(), localizer["Error403"]);
                }

                string[] alreadySended = await this.smsRepository.GetAlreadySendedSmsAsync(inviteMessage.PhoneNumbers);
                inviteMessage.PhoneNumbers = inviteMessage.PhoneNumbers.Except(alreadySended).ToArray();
                if (!inviteMessage.PhoneNumbers.Any())
                {
                    throw new ApiException(((int)HttpStatusCode.NotFound).ToString(), localizer["Error404"]);
                }

                await this.SendSmsAsync(inviteMessage.PhoneNumbers, inviteMessage.TranslatedMessage);

                return await this.smsRepository.InviteAsync(userId, inviteMessage.PhoneNumbers);
            }
            catch(ApiException)
            {
                throw;
            }
            catch(Exception ex)
            {
                this.logger.LogError(ex, "moduleName");
                throw new ApiException(((int)HttpStatusCode.InternalServerError).ToString(), ex.Message, moduleName);
            }            
        }

        private async Task SendSmsAsync(string[] phoneNumbers, string message)
        {
            Console.WriteLine(message);
            // SmsSender.SendAsync(phoneNumbers, message);
        }
    }
}
