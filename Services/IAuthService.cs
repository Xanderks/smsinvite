﻿namespace SmsInvite.Services
{
    using SmsInvite.Models;
    using System.Threading.Tasks;

    /// <summary>
    /// Auth service
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// Sends the invites asynchronous.
        /// </summary>
        /// <param name="inviteMessage">The invite message.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<int> SendInvitesAsync(InviteMessageModel inviteMessage, int userId);
    }
}
