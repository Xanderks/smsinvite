﻿namespace SmsInvite.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using SmsInvite.Models;
    using SmsInvite.Services;
    using System.Threading.Tasks;

    /// <summary>
    /// Auth methods
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class AuthController : BaseController
    {
        private readonly ILogger<AuthController> logger;
        private readonly IAuthService authService;

        public AuthController(ILogger<AuthController> logger, IAuthService authService)
        {
            this.logger = logger;
            this.authService = authService;
        }

        /// <summary>
        /// Sends the invites.
        /// </summary>
        /// <param name="phoneNumbers">The phone numbers.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        [HttpPost("send-invites")]
        public async Task<ActionResult> SendInvitesAsync([FromQuery]string[] phoneNumbers, [FromQuery] string message)
        {
            InviteMessageModel model = new InviteMessageModel { PhoneNumbers = phoneNumbers, Message = message };
            int invitationId = await this.authService.SendInvitesAsync(model, this.GetCurrentUser());

            return new CreatedResult($"/invitation/{invitationId}", "");
        }
    }
}
