﻿
namespace SmsInvite.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Base service controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// Gets the get current user (mock).
        /// </summary>
        /// <value>
        /// The get current user.
        /// </value>
        [NonAction]
        public int GetCurrentUser() => 777;
    }
}
