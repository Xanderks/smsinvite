﻿namespace SmsInvite.DAL
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// Mock DB repo
    /// </summary>
    /// <seealso cref="SmsInvite.DAL.ISmsRepository" />
    public class SmsRepository : ISmsRepository
    {
        public async Task<int> GetCountInvitationsAsync()
        {
            //Call FUNCTION getcountinvitations(apiid integer)

            return new Random().Next(0, 200);
        }

        public async Task<int> InviteAsync(int userId, string[] phones)
        {
            //Call Invite(user_id integer, phones text[])

            return new Random().Next(0, int.MaxValue);
        }

        public async Task<string[]> GetAlreadySendedSmsAsync(string[] phones)
        {
            //select phone from invitation where phone in (:phones);

            return new string[] { };
        }
    }
}
