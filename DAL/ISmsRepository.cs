﻿namespace SmsInvite.DAL
{
    using System.Threading.Tasks;

    /// <summary>
    /// SMS repo
    /// </summary>
    public interface ISmsRepository
    {
        /// <summary>
        /// Gets the count invitations asynchronous.
        /// </summary>
        /// <returns></returns>
        /// 
        Task<int> GetCountInvitationsAsync();
        /// <summary>
        /// Invites the asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="phones">The phones.</param>
        /// <returns></returns>
        Task<int> InviteAsync(int userId, string[] phones);

        /// <summary>
        /// Gets the already sended SMS asynchronous.
        /// </summary>
        /// <param name="phones">The phones.</param>
        /// <returns></returns>
        Task<string[]> GetAlreadySendedSmsAsync(string[] phones);
    }
}
